package com.example.exam.repositories;

import com.example.exam.entities.CheckPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CheckRepository extends JpaRepository<CheckPayment,Long> {
}
