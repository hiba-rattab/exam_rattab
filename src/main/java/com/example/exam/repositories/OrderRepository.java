package com.example.exam.repositories;

import com.example.exam.entities.OrderEntity;
import com.example.exam.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity,Long> {
}
