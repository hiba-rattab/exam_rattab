package com.example.exam.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
@Entity
public class CheckPayment extends Payment{

    private String name;
    private String bankID;

    public CheckPayment() {
    }

    public CheckPayment(Float amount, String name, String bankID) {
        super(amount);
        this.name = name;
        this.bankID = bankID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }


    public boolean authorized(){
        return !name.isEmpty() && !bankID.isEmpty();
    }
}
