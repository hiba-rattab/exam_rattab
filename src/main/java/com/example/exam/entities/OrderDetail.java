package com.example.exam.entities;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderdetailId;
    private Long quantity;
    private String taxStatus;
    @ManyToOne
    @JoinColumn(name = "orderId")
    private OrderEntity orders2;
    @ManyToOne
    @JoinColumn(name = "itemId")
    private Item item;
    public OrderDetail() {
    }

    public OrderDetail(Long quantity, String taxStatus) {
        this.quantity = quantity;
        this.taxStatus = taxStatus;
    }

    public Long getOrderdetailId() {
        return orderdetailId;
    }

    public void setOrderdetailId(Long orderdetailId) {
        this.orderdetailId = orderdetailId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }


    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }


}
