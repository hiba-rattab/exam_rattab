package com.example.exam.entities;

import jakarta.persistence.Entity;

@Entity
public class Cash extends Payment{
    private Float cashTendered;

    public Cash() {
    }

    public Cash(Float cashTendered) {
        this.cashTendered = cashTendered;
    }

    public Cash(Float amount, Float cashTendered) {
        super(amount);
        this.cashTendered = cashTendered;
    }

    public Float getCashTendered() {
        return cashTendered;
    }

    public void setCashTendered(Float cashTendered) {
        this.cashTendered = cashTendered;
    }
}
