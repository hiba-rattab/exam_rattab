package com.example.exam.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.PrimaryKeyJoinColumn;

import java.util.Date;

@Entity
public class Credit extends Payment{
    private String number;
    private String type;
    private Date expDate;

    public Credit() {
    }

    public Credit(Float amount, String number, String type,Date expDate) {
        super(amount);
        this.number = number;
        this.type = type;
        this.expDate = expDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public boolean authorized(){
        return !number.isEmpty() && !type.isEmpty();
    }
}
