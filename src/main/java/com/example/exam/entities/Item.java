package com.example.exam.entities;

import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long itemId;
    private Float shippingweight;
    private String description;
    @OneToMany(mappedBy = "item",fetch = FetchType.EAGER)
    private List<OrderDetail> details;
    public Item() {
    }

    public Item(Float shippingweight, String description) {
        this.shippingweight = shippingweight;
        this.description = description;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Float getShippingweight() {
        return shippingweight;
    }

    public void setShippingweight(Float shippingweight) {
        this.shippingweight = shippingweight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(itemId, item.itemId) && Objects.equals(shippingweight, item.shippingweight) && Objects.equals(description, item.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, shippingweight, description);
    }
}
