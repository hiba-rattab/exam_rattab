package com.example.exam.entities;



import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    private Date date;
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "orders2")
    private List<OrderDetail> details;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "orderpayment")
    private List<Payment> payments;

    public OrderEntity() {
    }

    public OrderEntity(Date date, Status status) {
        this.date = date;
        this.status = status;
    }

    public OrderEntity(Date date, Status status, List<OrderDetail> details, Customer customer, List<Payment> payments) {
        this.date = date;
        this.status = status;
        this.details = details;
        this.customer = customer;
        this.payments = payments;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
}
