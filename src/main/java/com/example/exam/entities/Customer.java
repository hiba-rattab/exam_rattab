package com.example.exam.entities;

import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long costumerId;
    private String name;
    private String adresss;

    @OneToMany(mappedBy = "customer",fetch = FetchType.EAGER)
    private List<OrderEntity> orders;
    public Customer() {
    }

    public Customer(String name, String adresss) {
        this.name = name;
        this.adresss = adresss;
    }

    public Long getCostumerId() {
        return costumerId;
    }

    public void setCostumerId(Long costumerId) {
        this.costumerId = costumerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresss() {
        return adresss;
    }

    public void setAdresss(String adresss) {
        this.adresss = adresss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(costumerId, customer.costumerId) && Objects.equals(name, customer.name) && Objects.equals(adresss, customer.adresss);
    }

    @Override
    public int hashCode() {
        return Objects.hash(costumerId, name, adresss);
    }
}
