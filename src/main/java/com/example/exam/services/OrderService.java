package com.example.exam.services;

import com.example.exam.entities.OrderEntity;
import com.example.exam.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    private OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;

    }

    public List<OrderEntity>  getallorders(){
        return orderRepository.findAll();
    }

    public Optional<OrderEntity> getorderbyid(Long orderid){
        return orderRepository.findById(orderid);
    }

    public OrderEntity createorder(OrderEntity o){
        return orderRepository.save(o);
    }
    public void updateorder(Long orderid, OrderEntity o){
        orderRepository.findById(orderid)
                .map(order -> {
                    order.setOrderId(o.getOrderId());
                    order.setDate(o.getDate());
                    order.setStatus(o.getStatus());
                    return orderRepository.save(order);
                })
                .orElseGet(()->orderRepository.save(o));

    }
    public void deleteorder(Long orderid){
        orderRepository.deleteById(orderid);
    }


}
