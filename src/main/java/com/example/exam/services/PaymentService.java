package com.example.exam.services;


import com.example.exam.entities.Payment;
import com.example.exam.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentService {
    private PaymentRepository paymentRepository;
    @Autowired
    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }


    public List<Payment> getallpayments(){
        return paymentRepository.findAll();
    }

    public Optional<Payment> getpaymentbyid(Long paymentid){
        return paymentRepository.findById(paymentid);
    }

    public Payment createpayment(Payment p){
        return paymentRepository.save(p);
    }
    public void updatepayment(Long paymentid, Payment p){
        paymentRepository.findById(paymentid)
                .map(payment -> {
                    payment.setPaymentId(p.getPaymentId());
                    payment.setAmount(p.getAmount());
                    return paymentRepository.save(payment);
                })
                .orElseGet(()->paymentRepository.save(p));

    }
    public void deletepayment(Long paymentid){
        paymentRepository.deleteById(paymentid);
    }
}
