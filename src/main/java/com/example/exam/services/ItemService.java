package com.example.exam.services;

import com.example.exam.entities.Item;
import com.example.exam.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    private ItemRepository itemRepository;
    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }


    public List<Item> getallitems(){
        return itemRepository.findAll();
    }

    public Optional<Item> getitembyid(Long itemid){
        return itemRepository.findById(itemid);
    }

    public Item createitem(Item i){
        return itemRepository.save(i);
    }
    public void updateitem(Long itemid, Item i){
        itemRepository.findById(itemid)
                .map(item -> {
                    item.setItemId(i.getItemId());
                    item.setShippingweight(i.getShippingweight());
                    item.setDescription(i.getDescription());
                    return itemRepository.save(item);
                })
                .orElseGet(()->itemRepository.save(i));

    }
    public void deleteitem(Long itemId){
        itemRepository.deleteById(itemId);
    }


}
