package com.example.exam.services;

import com.example.exam.entities.Cash;
import com.example.exam.repositories.CashRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CashService {
    private CashRepository cashRepository;
    @Autowired
    public CashService(CashRepository cashRepository) {
        this.cashRepository = cashRepository;
    }
    public List<Cash> getallcashes(){
        return cashRepository.findAll();
    }

    public Optional<Cash> getcashbyid(Long cashid){
        return cashRepository.findById(cashid);
    }

    public Cash createcash(Cash c){
        return cashRepository.save(c);
    }
    public void updatecash(Long cashid, Cash c){
        cashRepository.findById(cashid)
                .map(cash -> {
                    cash.setCashTendered(c.getCashTendered());
                    return cashRepository.save(cash);
                })
                .orElseGet(()->cashRepository.save(c));

    }
    public void deletecash(Long cashId){
        cashRepository.deleteById(cashId);
    }
}
