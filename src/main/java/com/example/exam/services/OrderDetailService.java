package com.example.exam.services;

import com.example.exam.entities.Item;
import com.example.exam.entities.OrderDetail;
import com.example.exam.repositories.OrderDetailRepository;
import jakarta.persistence.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderDetailService {
    private OrderDetailRepository orderDetailRepository;
    @Autowired
    public OrderDetailService(OrderDetailRepository orderDetailRepository) {
        this.orderDetailRepository = orderDetailRepository;
    }



    public List<OrderDetail> getallorderdetails(){
        return orderDetailRepository.findAll();
    }

    public Optional<OrderDetail> getorderdetailbyid(Long orderdetid){
        return orderDetailRepository.findById(orderdetid);
    }

    public OrderDetail createorderdetail(OrderDetail o){
        return orderDetailRepository.save(o);
    }
    public void updateorderdet(Long orderdetid, OrderDetail o){
        orderDetailRepository.findById(orderdetid)
                .map(orderdet -> {
                    orderdet.setOrderdetailId(o.getOrderdetailId());
                    orderdet.setQuantity(o.getQuantity());
                    orderdet.setTaxStatus(o.getTaxStatus());
                    return orderDetailRepository.save(orderdet);
                })
                .orElseGet(()->orderDetailRepository.save(o));

    }
    public void deleteorderdetail(Long orderid){
        orderDetailRepository.deleteById(orderid);
    }


    public Float calcWeight(Item i){
        return i.getShippingweight();
    }
}
