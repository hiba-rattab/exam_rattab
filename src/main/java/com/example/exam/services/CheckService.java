package com.example.exam.services;

import com.example.exam.entities.CheckPayment;
import com.example.exam.repositories.CheckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CheckService {
    private CheckRepository checkRepository;
    @Autowired

    public CheckService(CheckRepository checkRepository) {
        this.checkRepository = checkRepository;
    }

    public List<CheckPayment> getallchecks(){
        return checkRepository.findAll();
    }

    public Optional<CheckPayment> getcheckbyid(Long checkid){
        return checkRepository.findById(checkid);
    }

    public CheckPayment createcheck(CheckPayment c){
        return checkRepository.save(c);
    }
    public void updatecheck(Long checkid, CheckPayment c){
        checkRepository.findById(checkid)
                .map(checkPayment -> {
                    checkPayment.setName(c.getName());
                    checkPayment.setBankID(c.getBankID());
                    return checkRepository.save(checkPayment);
                })
                .orElseGet(()->checkRepository.save(c));

    }
    public void deletecheck(Long checkId){
        checkRepository.deleteById(checkId);
    }
}
