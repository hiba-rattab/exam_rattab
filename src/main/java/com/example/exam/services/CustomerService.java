package com.example.exam.services;

import com.example.exam.entities.Customer;
import com.example.exam.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;
    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getallcustomers(){
        return customerRepository.findAll();
    }

    public Optional<Customer> getcustomerbyid(Long customerid){
        return customerRepository.findById(customerid);
    }

    public Customer createcustomer(Customer c){
        return customerRepository.save(c);
    }
    public void updatecustomer(Long customerid, Customer c){
        customerRepository.findById(customerid)
                .map(customer -> {
                    customer.setCostumerId(c.getCostumerId());
                    customer.setName(c.getName());
                    customer.setAdresss(c.getAdresss());
                    return customerRepository.save(customer);
                })
                .orElseGet(()->customerRepository.save(c));

    }
    public void deletecustomer(Long customerId){
        customerRepository.deleteById(customerId);
    }



}
