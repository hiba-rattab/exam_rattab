package com.example.exam.services;

import com.example.exam.entities.Credit;
import com.example.exam.entities.Customer;
import com.example.exam.repositories.CreditRepository;
import com.example.exam.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CreditService {

    private CreditRepository creditRepository;
    @Autowired
    public CreditService(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    public List<Credit> getallcredits(){
        return creditRepository.findAll();
    }

    public Optional<Credit> getcreditbyid(Long creditid){
        return creditRepository.findById(creditid);
    }

    public Credit createcredit(Credit c){
        return creditRepository.save(c);
    }
    public void updatecredit(Long creditid, Credit c){
        creditRepository.findById(creditid)
                .map(credit -> {
                    credit.setNumber(c.getNumber());
                    credit.setType(c.getType());
                    credit.setExpDate(c.getExpDate());
                    return creditRepository.save(credit);
                })
                .orElseGet(()->creditRepository.save(c));

    }
    public void deletecredit(Long creditId){
        creditRepository.deleteById(creditId);
    }


}
