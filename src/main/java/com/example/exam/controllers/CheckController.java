package com.example.exam.controllers;


import com.example.exam.entities.CheckPayment;
import com.example.exam.services.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/checks")
public class CheckController {

    private CheckService checkService;
    @Autowired
    public CheckController(CheckService checkService) {
        this.checkService = checkService;
    }



    @GetMapping
    public List<CheckPayment> getchecks(){

        return checkService.getallchecks();
    }

    @GetMapping("/{id}")
    public Optional<CheckPayment> getcheckbyid(@PathVariable Long id){
        return checkService.getcheckbyid(id);
    }

    @PostMapping
    public CheckPayment createcheck(@RequestBody CheckPayment check){
        return checkService.createcheck(check);
    }

    @PutMapping("/{id}")
    public void updatecheck(@PathVariable Long id, @RequestBody CheckPayment c){
        checkService.updatecheck(id,c);
    }
    @DeleteMapping("/{id}")
    public void deletecheck(@PathVariable Long id){
        checkService.deletecheck(id);
    }
}
