package com.example.exam.controllers;

import com.example.exam.entities.OrderDetail;
import com.example.exam.entities.OrderEntity;
import com.example.exam.services.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/details")
public class OrderDetailController {

    private OrderDetailService orderDetailService;

    @Autowired
    public OrderDetailController(OrderDetailService orderDetailService) {
        this.orderDetailService = orderDetailService;
    }

    @GetMapping
    public List<OrderDetail> getorderdets(){

        return orderDetailService.getallorderdetails();
    }

    @GetMapping("/{id}")
    public Optional<OrderDetail> getorderdetbyid(@PathVariable Long id){
        return orderDetailService.getorderdetailbyid(id);
    }

    @PostMapping
    public OrderDetail createOrder(@RequestBody OrderDetail order){
        return orderDetailService.createorderdetail(order);
    }

    @PutMapping("/{id}")
    public void updateorder(@PathVariable Long id, @RequestBody OrderDetail o){
        orderDetailService.updateorderdet(id,o);
    }
    @DeleteMapping("/{id}")
    public void deleteorder(@PathVariable Long id){
        orderDetailService.deleteorderdetail(id);
    }

}
