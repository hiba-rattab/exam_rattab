package com.example.exam.controllers;

import com.example.exam.entities.Item;
import com.example.exam.entities.OrderEntity;
import com.example.exam.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/items")
public class ItemController {

    private ItemService itemService;
    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping
    public List<Item> getitems(){

        return itemService.getallitems();
    }

    @GetMapping("/{id}")
    public Optional<Item> getitembyid(@PathVariable Long id){
        return itemService.getitembyid(id);
    }

    @PostMapping
    public Item createitem(@RequestBody Item item){
        return itemService.createitem(item);
    }

    @PutMapping("/{id}")
    public void updateitem(@PathVariable Long id, @RequestBody Item i){
        itemService.updateitem(id,i);
    }
    @DeleteMapping("/{id}")
    public void deleteitem(@PathVariable Long id){
        itemService.deleteitem(id);
    }


}
