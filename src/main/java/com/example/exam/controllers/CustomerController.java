package com.example.exam.controllers;

import com.example.exam.entities.Customer;
import com.example.exam.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;
    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    @GetMapping
    public List<Customer> getitems(){

        return customerService.getallcustomers();
    }

    @GetMapping("/{id}")
    public Optional<Customer> getcustumerbyid(@PathVariable Long id){
        return customerService.getcustomerbyid(id);
    }

    @PostMapping
    public Customer createcustomer(@RequestBody Customer customer){
        return customerService.createcustomer(customer);
    }

    @PutMapping("/{id}")
    public void updatecustomer(@PathVariable Long id, @RequestBody Customer c){
        customerService.updatecustomer(id,c);
    }
    @DeleteMapping("/{id}")
    public void deletecustomer(@PathVariable Long id){
        customerService.deletecustomer(id);
    }


}
