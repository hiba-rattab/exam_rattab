package com.example.exam.controllers;


import com.example.exam.entities.Cash;
import com.example.exam.services.CashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cashes")
public class CashController {
    private CashService cashService;
    @Autowired
    public CashController(CashService cashService) {
        this.cashService = cashService;
    }



    @GetMapping
    public List<Cash> getcashes(){
        return cashService.getallcashes();
    }

    @GetMapping("/{id}")
    public Optional<Cash> getcashbyid(@PathVariable Long id){
        return cashService.getcashbyid(id);
    }

    @PostMapping
    public Cash createcash(@RequestBody Cash cash){
        return cashService.createcash(cash);
    }

    @PutMapping("/{id}")
    public void updatecash(@PathVariable Long id, @RequestBody Cash c){
        cashService.updatecash(id,c);
    }
    @DeleteMapping("/{id}")
    public void deletecash(@PathVariable Long id){
        cashService.deletecash(id);
    }
}
