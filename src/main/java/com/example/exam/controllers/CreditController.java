package com.example.exam.controllers;

import com.example.exam.entities.Credit;
import com.example.exam.entities.Customer;
import com.example.exam.services.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/credits")
public class CreditController {
    private CreditService creditService;
    @Autowired
    public CreditController(CreditService creditService) {
        this.creditService = creditService;
    }


    @GetMapping
    public List<Credit> getcredits(){

        return creditService.getallcredits();
    }

    @GetMapping("/{id}")
    public Optional<Credit> getcreditbyid(@PathVariable Long id){
        return creditService.getcreditbyid(id);
    }

    @PostMapping
    public Credit createcredit(@RequestBody Credit credit){
        return creditService.createcredit(credit);
    }

    @PutMapping("/{id}")
    public void updatecredit(@PathVariable Long id, @RequestBody Credit c){
        creditService.updatecredit(id,c);
    }
    @DeleteMapping("/{id}")
    public void deletecredit(@PathVariable Long id){
        creditService.deletecredit(id);
    }
}
