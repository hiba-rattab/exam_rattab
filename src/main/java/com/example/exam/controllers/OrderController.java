package com.example.exam.controllers;

import com.example.exam.entities.OrderEntity;
import com.example.exam.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
public class OrderController {
    private OrderService orderService;
    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    public List<OrderEntity> getorders(){

        return orderService.getallorders();
    }

    @GetMapping("/{id}")
    public Optional<OrderEntity> getorderbyid(@PathVariable Long id){
        return orderService.getorderbyid(id);
    }

    @PostMapping
    public OrderEntity createOrder(@RequestBody OrderEntity order){
        return orderService.createorder(order);
    }

    @PutMapping("/{id}")
    public void updateorder(@PathVariable Long id, @RequestBody OrderEntity o){
        orderService.updateorder(id,o);
    }
    @DeleteMapping("/{id}")
    public void deleteorder(@PathVariable Long id){
        orderService.deleteorder(id);
    }
}
