package com.example.exam.controllers;

import com.example.exam.entities.OrderEntity;
import com.example.exam.entities.Payment;
import com.example.exam.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/payments")
public class PaymentController {
    private PaymentService paymentService;
    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }
    @GetMapping
    public List<Payment> getpayments(){
        return paymentService.getallpayments();
    }

    @GetMapping("/{id}")
    public Optional<Payment> getpaymentbyid(@PathVariable Long id){
        return paymentService.getpaymentbyid(id);
    }

    @PostMapping
    public Payment createpayment(@RequestBody  Payment p){
        return paymentService.createpayment(p);
    }

    @PutMapping("/{id}")
    public void updatepayment(@PathVariable Long id, @RequestBody Payment p){
        paymentService.updatepayment(id,p);
    }
    @DeleteMapping("/{id}")
    public void deletepayment(@PathVariable Long id){
        paymentService.deletepayment(id);
    }

}
